<?php

namespace Drupal\back_to\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Core\Link;

/**
 * Provides a block to display the back link.
 *
 * @Block(
 *   id = "back_to_block",
 *   admin_label = @Translation("Back To")
 * )
 */
class BackToBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The breadcrumb manager.
   *
   * @var \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface
   */
  protected $breadcrumbManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new BackToBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface $breadcrumb_manager
   *   The breadcrumb manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, BreadcrumbBuilderInterface $breadcrumb_manager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->breadcrumbManager = $breadcrumb_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('breadcrumb'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'text_prefix' => $this->t('Back To'),
      'link_attributes' => ['class' => '']
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    if (!\Drupal::moduleHandler()->moduleExists('yaml_editor')) {
      $message = $this->t('It is recommended to install the <a href="@yaml-editor">YAML Editor</a> module for easier editing.', [
        '@yaml-editor' => 'https://www.drupal.org/project/yaml_editor',
      ]);

      drupal_set_message($message, 'warning');
    }

    $link_attributes_yaml = Yaml::encode($this->configuration['link_attributes']);

    $form['setting'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Setting'),
    ];

    $form['setting']['text_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text prefix'),
      '#default_value' => $this->configuration['text_prefix'],
    ];

    $form['setting']['link_attributes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Link attributes'),
      '#default_value' => $link_attributes_yaml,
      '#attributes' => ['data-yaml-editor' => 'true'],
      '#rows' => 15,
      '#description' => $this->t('Available attributes can be defined in YAML syntax.'),
    ];

    /*$form['setting']['link_attributes']['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link class'),
      '#default_value' => $this->configuration['link_attributes']['class'],
    ];*/

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $setting = $form_state->getValue('setting');

    try {
      $form_state->setValue(['setting', 'link_attributes'], Yaml::decode($setting['link_attributes']));
    }
    catch (InvalidDataTypeException $e) {
      $form_state->setErrorByName('setting][link_attributes', $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $setting = $form_state->getValue('setting');

    $this->configuration['text_prefix'] = $setting['text_prefix'];
    $this->configuration['link_attributes'] = $setting['link_attributes'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $links = $this->breadcrumbManager->build($this->routeMatch)->getLinks();

    if (!empty($links)) {
      /** @var Link $link */
      $link = end($links);
      $link->setText($this->configuration['text_prefix'] . ' ' . $link->getText());

      if (!empty($this->configuration['link_attributes'])) {
        $link->getUrl()
          ->setOption('attributes', $this->configuration['link_attributes']);
      }

      return $link->toRenderable();
    }

    return NULL;
  }

}
